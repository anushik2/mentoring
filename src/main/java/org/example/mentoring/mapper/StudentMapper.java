package org.example.mentoring.mapper;

import org.mapstruct.Mapper;
import org.example.mentoring.dto.UserDto;
import org.example.mentoring.model.StudentEntity;

@Mapper(componentModel = "spring")
public interface StudentMapper {
    StudentEntity toEntity(UserDto userDto);
    UserDto toDto(StudentEntity userEntity);
}
