package org.example.mentoring.mapper;

import org.example.mentoring.model.UserEntity;
import org.mapstruct.Mapper;
import org.example.mentoring.dto.UserDto;
import org.example.mentoring.model.AdminEntity;

@Mapper(componentModel = "spring")
public interface AdminMapper {
    AdminEntity toEntity(UserDto userDto);
    UserDto toDto(AdminEntity userEntity);
}
