package org.example.mentoring.mapper;

import org.example.mentoring.model.MentorEntity;
import org.mapstruct.Mapper;
import org.example.mentoring.dto.UserDto;
import org.example.mentoring.model.AdminEntity;

@Mapper(componentModel = "spring")
public interface MentorMapper {
    MentorEntity toEntity(UserDto userDto);
    UserDto toDto(MentorEntity userEntity);
}
