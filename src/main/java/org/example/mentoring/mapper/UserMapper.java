package org.example.mentoring.mapper;

import org.example.mentoring.dto.UserDto;
import org.example.mentoring.model.UserEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserEntity toEntity(UserDto userDto);
    UserDto toDto(UserEntity userEntity);
}

