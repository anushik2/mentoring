package org.example.mentoring.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import java.util.List;

@Entity
@Data
@PrimaryKeyJoinColumn(name = "id")
public class MentorEntity extends UserEntity{
    @OneToMany(mappedBy = "mentor")
    private List<StudentEntity> students;
}
