package org.example.mentoring.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@Data
@PrimaryKeyJoinColumn(name = "id")
public class StudentEntity extends UserEntity{
    @ManyToOne
    @JoinColumn(name = "mentor_id")
    private MentorEntity mentor;
}
