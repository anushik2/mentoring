package org.example.mentoring.model;

import javax.persistence.Entity;

//@Entity
public enum Role {
    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_STUDENT("ROLE_STUDENT"),
    ROLE_MENTOR("ROLE_MENTOR");


    private String value;

    private Role(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
