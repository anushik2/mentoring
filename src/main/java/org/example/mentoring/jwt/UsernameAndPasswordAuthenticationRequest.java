package org.example.mentoring.jwt;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
@Setter
// petq a chlini
public class UsernameAndPasswordAuthenticationRequest {
    private String username;
    private String password;
}
