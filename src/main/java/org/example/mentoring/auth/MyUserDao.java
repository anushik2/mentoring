package org.example.mentoring.auth;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Repository;

import java.util.Optional;

public interface MyUserDao {
    Optional<MyUserDetails> findUserByUsername(String username);
}
