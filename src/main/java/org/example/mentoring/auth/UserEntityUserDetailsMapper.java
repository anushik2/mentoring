package org.example.mentoring.auth;

import org.example.mentoring.model.UserEntity;
import org.mapstruct.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class UserEntityUserDetailsMapper {
    public MyUserDetails map(UserEntity user) {
        HashSet<GrantedAuthority> role = new HashSet<>();
        role.add(new SimpleGrantedAuthority(user.getRole().toString()));
        return new MyUserDetails(user.getUsername(), user.getPassword(), role);
    }
}
