package org.example.mentoring.controller;

import lombok.RequiredArgsConstructor;
import org.example.mentoring.dto.UserDto;
import org.example.mentoring.service.AdminService;
import org.example.mentoring.service.StudentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @GetMapping("/register")
    public String registerAdmin() {
        return "register student";
    }

    @PostMapping("/register")
    public ResponseEntity<UserDto> registerStudent(@RequestBody UserDto userDto) {
        return new ResponseEntity<UserDto>(studentService.saveStudent(userDto), HttpStatus.CREATED);
    }

//    @GetMapping("/welcome")
//    public String welcome() {
//        return "student welcome";
//    }
}
