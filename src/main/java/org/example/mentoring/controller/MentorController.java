package org.example.mentoring.controller;

import lombok.RequiredArgsConstructor;
import org.example.mentoring.dto.UserDto;
import org.example.mentoring.service.AdminService;
import org.example.mentoring.service.MentorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mentor")
@RequiredArgsConstructor
public class MentorController {
    private final MentorService mentorService;

    @GetMapping("/register")
    public String registerMentor() {
        return "register Mentor";
    }

    @PostMapping("/register")
    public ResponseEntity<UserDto> registerMentor(@RequestBody UserDto user) {
        return new ResponseEntity<>(mentorService.saveMentor(user), HttpStatus.CREATED);
    }

//    @GetMapping("/welcome")
//    public String welcome() {
//        return "mentor welcome";
//    }
}
