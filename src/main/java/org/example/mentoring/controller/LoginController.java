package org.example.mentoring.controller;

import org.apache.catalina.User;
import org.example.mentoring.dto.UserDto;
import org.example.mentoring.jwt.UsernameAndPasswordAuthenticationRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class LoginController {
    @GetMapping("/login")
    public String login() {
        return "login";
    }

//    @PostMapping(value = "/login",consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
//    public ResponseEntity<HttpStatus> login(UsernameAndPasswordAuthenticationRequest user) {
//        System.out.println("login user");
//
//        return new ResponseEntity<> (HttpStatus.OK);
//    }


    @GetMapping("/welcome")
    public String welcome() {
        return "welcome";
    }
}
