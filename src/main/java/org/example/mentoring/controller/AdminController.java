package org.example.mentoring.controller;

import lombok.RequiredArgsConstructor;
import org.example.mentoring.dto.UserDto;
import org.example.mentoring.model.UserEntity;
import org.example.mentoring.service.AdminService;
import org.example.mentoring.service.MentorService;
import org.example.mentoring.service.StudentService;
import org.example.mentoring.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminController {
    private final AdminService adminService;
    private final StudentService studentService;
    private final MentorService mentorService;
    private final UserService userService;

    @GetMapping("/register")
    public String registerAdmin() {
        return "register admin";
    }

    @PostMapping("/register")
    public ResponseEntity<UserDto> registerAdmin(@RequestBody UserDto userDto) {
        System.out.println("register admin");
        return new ResponseEntity<UserDto>(adminService.saveAdmin(userDto),HttpStatus.CREATED);
    }

    @GetMapping("/create/mentor")
    public String createMentor() {
        return "create mentor";
    }

    @PostMapping("/create/mentor")
    public ResponseEntity<UserDto> createMentor(@RequestBody UserDto userDto) {
        return new ResponseEntity<UserDto>(mentorService.saveMentor(userDto),HttpStatus.CREATED);
    }

    @GetMapping("/create/student")
    public String createStudent() {
        return "create mentor";
    }

    @PostMapping("/create/student")
    public ResponseEntity<UserDto> createStudent(@RequestBody UserDto userDto) {
        return new ResponseEntity<>(studentService.saveStudent(userDto),HttpStatus.CREATED);
    }

    @GetMapping("/delete/student")
    public String deleteStudent() {
        return "delete mentor";
    }

    @PostMapping("/delete/student")
    public ResponseEntity<UserEntity> deleteStudent(@RequestParam Integer id) {
        System.out.println("in func");
        return new ResponseEntity<>(userService.deleteUser(id),HttpStatus.OK);
    }

    @GetMapping("/get/students")
    public ResponseEntity<List<UserDto>> getStudents() {
        System.out.println("get students");
        return new ResponseEntity<>(studentService.getStudents(), HttpStatus.OK);
    }

    @GetMapping("/get/mentors")
    public ResponseEntity<List<UserDto>> getMentors() {
        return new ResponseEntity<>(mentorService.getMentors(), HttpStatus.OK);
    }

//    @GetMapping("/welcome")
//    public String welcome() {
//        return "admin welcome";
//    }

}
