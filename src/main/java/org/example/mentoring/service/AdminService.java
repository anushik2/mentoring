package org.example.mentoring.service;

import lombok.RequiredArgsConstructor;
import org.example.mentoring.exception.UserAlredyExist;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.example.mentoring.model.*;
import org.example.mentoring.dto.UserDto;
import org.example.mentoring.repo.AdminRepo;
import org.example.mentoring.mapper.AdminMapper;


@Service
@RequiredArgsConstructor
@Transactional //????
public class AdminService  extends UserEntity {
    private final AdminRepo adminRepo;
    private final AdminMapper adminMapper;
    private final PasswordEncoder passwordEncoder;

    public UserDto saveAdmin(UserDto userDto) {
        if(!adminRepo.existsByEmail(userDto.getEmail())){
            AdminEntity user = adminMapper.toEntity(userDto);
            user.setRole(Role.ROLE_ADMIN);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            return adminMapper.toDto(adminRepo.save(user));
        }

        throw new UserAlredyExist("email already exist");

    }
}
