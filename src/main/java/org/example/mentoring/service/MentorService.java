package org.example.mentoring.service;

import lombok.RequiredArgsConstructor;
import org.example.mentoring.dto.UserDto;
import org.example.mentoring.exception.UserAlredyExist;
import org.example.mentoring.mapper.MentorMapper;
import org.example.mentoring.model.MentorEntity;
import org.example.mentoring.model.Role;
import org.example.mentoring.model.UserEntity;
import org.example.mentoring.repo.MentorRepo;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MentorService extends UserEntity {
    private final MentorRepo mentorRepo;
    private final MentorMapper mentorMapper;
    private final PasswordEncoder passwordEncoder;

    public UserDto saveMentor(UserDto userDto) {
        if(!mentorRepo.existsByEmail(userDto.getEmail())) {
            MentorEntity user = mentorMapper.toEntity(userDto);
            user.setRole(Role.ROLE_MENTOR);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            return mentorMapper.toDto(mentorRepo.save(user));
        }
        throw new UserAlredyExist("email already exist");
    }

    public List<UserDto> getMentors() {
        List<UserDto> userDtos = new ArrayList<>();
        List<MentorEntity> userEntities = mentorRepo.findAll();

        for (MentorEntity userEntity : userEntities) {
            userDtos.add(mentorMapper.toDto(userEntity));
        }
        return userDtos;
    }
}
