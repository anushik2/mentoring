package org.example.mentoring.service;

import lombok.RequiredArgsConstructor;
import org.example.mentoring.model.UserEntity;
import org.example.mentoring.repo.UserRepo;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    private  final UserRepo userRepo;
    public UserEntity deleteUser(Integer id) {
        Optional<UserEntity> user = userRepo.findById(id);

        if(user.isPresent()) {
            userRepo.deleteById(id);
            return user.get();
        }
        throw new UsernameNotFoundException(String.format("user with %s id doesn't exist", id));
    }
}
