package org.example.mentoring.service;

import lombok.RequiredArgsConstructor;
import org.example.mentoring.exception.UserAlredyExist;
import org.example.mentoring.model.UserEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.example.mentoring.mapper.StudentMapper;
import org.example.mentoring.repo.StudentRepo;
import org.example.mentoring.dto.UserDto;
import org.example.mentoring.model.Role;
import org.example.mentoring.model.StudentEntity;

import java.util.List;
import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class StudentService extends UserEntity {
    private final StudentRepo studentRepo;
    private final StudentMapper studentMapper;
    private final PasswordEncoder passwordEncoder;

    public UserDto saveStudent(UserDto userDto) {
        if(!studentRepo.existsByEmail(userDto.getEmail())) {
            StudentEntity user = studentMapper.toEntity(userDto);
            user.setRole(Role.ROLE_STUDENT);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            return studentMapper.toDto(studentRepo.save(user));
        }
        throw new UserAlredyExist("email already exist"); // TODO
    }

    public List<UserDto> getStudents() {
        List<UserDto> userDtos = new ArrayList<>();
        List<StudentEntity> userEntities = studentRepo.findAll();

        for (StudentEntity userEntity : userEntities) {
            userDtos.add(studentMapper.toDto(userEntity));
        }
        return userDtos;
    }
}
