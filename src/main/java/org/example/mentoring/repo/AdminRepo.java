package org.example.mentoring.repo;

import org.example.mentoring.model.AdminEntity;
import org.example.mentoring.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AdminRepo extends JpaRepository<AdminEntity, Integer>{
    List<AdminEntity> findAllByRole(Role role);

    boolean existsByEmail(String email);
}
