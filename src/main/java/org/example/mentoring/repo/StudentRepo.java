package org.example.mentoring.repo;

import java.util.List;

import org.example.mentoring.model.Role;
import org.example.mentoring.model.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface StudentRepo extends JpaRepository<StudentEntity, Integer> {
    List<StudentEntity> findAllByRole(Role role);

    boolean existsByEmail(String email);
}
