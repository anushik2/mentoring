package org.example.mentoring.repo;

import org.example.mentoring.model.MentorEntity;
import org.example.mentoring.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MentorRepo extends JpaRepository<MentorEntity, Integer> {
    boolean existsByEmail(String email);

    List<MentorEntity> findAllByRole(Role role);
}
