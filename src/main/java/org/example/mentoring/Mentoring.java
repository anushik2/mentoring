package org.example.mentoring;

import org.apache.naming.factory.BeanFactory;
import org.example.mentoring.model.StudentEntity;
import org.example.mentoring.model.UserEntity;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cglib.beans.BeanGenerator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableSwagger2WebMvc
@EnableAutoConfiguration
@EnableWebSecurity
public class Mentoring {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        SpringApplication.run(Mentoring.class, args);


        ApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext();
        Test bean = annotationConfigApplicationContext.getBean(Test.class);
        System.out.println(bean);


//        BeanGenerator beanGenerator = new BeanGenerator();
//
//        beanGenerator.addProperty("name", String.class);
//        Object myBean = beanGenerator.create();
//        Method setter = myBean.getClass().getMethod("setName", String.class);
//        setter.invoke(myBean, "some string value set by a cglib");
//
//        Method getter = myBean.getClass().getMethod("getName");
//        System.out.println("some string value set by a cglib" + getter.invoke(myBean));
    }
}
